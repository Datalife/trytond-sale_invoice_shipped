datalife_sale_invoice_shipped
=============================

The sale_invoice_shipped module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_invoice_shipped/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_invoice_shipped)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
